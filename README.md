# Node Utils

Repositório para colocar scripts para ajudar no desenvolvimento de aplicações em nodejs.

Na pasta [scipts](./scipts) fica os script que ajuda no desenvolvimento de API em nodejs, por exemplo:

[**api_template.sh**](./scipts/api_template.sh)
Ele cria automaticamente os seguintes diretórios:

## Estrutura de diretórios

📂 **Doc:** Diretório onde ficará a documentação da aplicação.

📂 **Src/App:** Diretório da aplicação, onde ficara os Controllers, os Models, os Middlewares e as Rotas da aplicação.

📂 **Src/App/Routes/index.js** possui o seguinte código, que serve para importar usando o require('caminho do arquivo')(app) automaticamente todos os arquivos (.js) de rotas que foram colcados neste diretório.

```js
"use strict";
const fs = require("fs");
const path = require("path");
/* Criando função para import automaticamente todos os arquivos js colocados neste diretório */
module.exports = app => {
    fs.readdirSync(__dirname)
        .filter(
            file =>
                file.indexOf(".") !== 0 &&
                file !== "index.js" &&
                path.extname(file) !== ".js"
        )
        .forEach(file => require(path.resolve(__dirname, file))(app));
};
```

no arquivo App.js basta fazer o seguinte para importar as rotas:

```js
const express = require("express");
const app = express();
require("./Routes")(app); // app o nome da variavel da aplicação, que pode ser outro
```

**Src/Config:** Diretório onde as configurações do projetos devem ficar, seja em arquivos .js ou .json .

📂 **Src/App/Config/default.config.js** Arquivo onde seram colocadas as configurações da aplicação, como a porta de execução, porta da base de dados, url da base de dados, etc.

📂 **Src/App/Config/default.error.js** Arquivo onde ficam as respoastas para erros comuns, como internal erros, usuário não encontrado, entre outros.

📂 **Src/Database:** Diretório onde fica o arquivo de conmunicação com o banco de dados.

📂 **Src/Utils:** Diretório onde ficam os aquivos de ferramentas, funcões, etc. Que ajudam a otmizar a quantidade de códigos que será escrita. Exemplo, caso tenha uma função que seja usando em varios arquivos, é melhor colocar ela apenas em um aruqivo e importar suas chamadas no arquivos necessários, evitando assim a repetição de código.

A árvore final ficará assim:

-   🏠 **root_project/**
    -   📂 **Doc/**
    -   📂 **node_modules/\***
    -   📂 **Src/**
        -   📂 **App/**
            -   📂 **Controllers/\***
            -   📂 **Middlwares/\***
            -   📂 **Models/User.js**
            -   📂 **Routes/index.js**
        -   📂 **Config/default.config.js**
        -   📂 **Config/default.errors.js**
        -   📂 **Database/index.js**
        -   📂 **Utils/index.js**

## Lista de modulos indicados

Para de desenvolver uma API em [Nodejs](https://nodejs.org/en/) indicamos o uso do [MongoDB](https://www.mongodb.com/) como base de dados padrão e para facilitar seu uso o [Mongoose](http://mongoosejs.com/).

**Lista Completa de Módulos:**

-   **bcryptjs**
-   **cors**
-   **compression** comprimir o cabeçalho da respostas e requisições
-   **express** framework para facilitar o uso do nodejs paara contruir a api, ele possui uma facilitação muito grande para identificação derotas, exemplo:

    ```javascript
    app.get("/auth", (req, res, next) => {
        /* pode ser usado o send também, mas o json ja envia em formato json. */
        return res.status(202).json({
            msg: "Olá!"
        });
    });
    ```

-   **mongoose** O Mongoose fornece uma solução direta e baseada em esquema para modelar os dados do seu aplicativo. Ele inclui conversão de tipo incorporada, validação, construção de consulta, ganchos de lógica de negócios e muito mais, fora da caixa.
-   **helmet** O Helmet ajuda você a proteger seus aplicativos Express configurando vários cabeçalhos HTTP. Não é uma bala de prata, mas pode ajudar!
-   **multer** Para upload de arquivos
-   **jsonwebtoken** Os JSON Web Tokens são um método RFC 7519 padrão da indústria, aberto, para representar reivindicações com segurança entre duas partes.
-   **minimist** para organização e reqcuperação de parametros/argumentos, exemplo:

    ```
    bash node index.js --port 3000
    ```
    - **nodemon**

    -**serve-favicon**
    85/5000
    Um favicon é uma sugestão visual que o software do cliente, como os navegadores, usa para identificar um site.

## Instalação

Para instalar estes módulos basta concordar com a pergunta feita pelo script de criação do template.

## Instalação do script (global ) no linux e OSX

```bash
git clone https://gitlab.com/r.maycon/node-utils.git
cd node-utils
sudo cp scripts/api_template /bin/apimake
sudo chmod +x /bin/apimake
```

## Executando

Para executar basta chamar pelo apelido no terminal

```
apimake
apimake --offline
```

--offline para instalar o modulos em modo offline, caso eles ja estejam presentes no cache do npm
